﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeController : MonoBehaviour {

    public Slider healthBar;
    public int maxHealth = 20;
    public int health;

    public GameObject player;

    private GameController gameController;
	void Start () {
        health = maxHealth;
        UpdateHealthBar();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        UpdateHealthBar();

        if (health <= 0)
        {
            Death();
        }

    }



    void Death()
    {
        gameController.GameOver();
        Destroy(gameObject);
        Destroy(player);
    }


	void UpdateHealthBar () {
        healthBar.value = health / (float)(maxHealth);
	}
}
